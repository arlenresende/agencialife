(function ($) {
    var oAddClass = $.fn.addClass;
    $.fn.addClass = function () {
        for (var i in arguments) {
            var arg = arguments[i];
            if ( !! (arg && arg.constructor && arg.call && arg.apply)) {
                setTimeout(arg.bind(this));
                delete arguments[i];
            }
        }
        return oAddClass.apply(this, arguments);
    };

})(jQuery);

function msieVersion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) /* If Internet Explorer, return version number */
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    else /* If another browser, return 0 */
        return 0;
}
function ie() {
    var ua = window.navigator.userAgent,
      msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    return false;
}

function ancora(){
    var off;
    var $doc = $('html, body');
    $('.ancora').click(function(e) {
        off = $(this).data('ancora') ? $(this).data('ancora') : 0;

        $doc.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - off
        }, 800);
        return false;
    });
}

function youtube(){
        
    var boxVideo = $(".youtube_video");
    var idVideo = boxVideo.data('video').replace("https://www.youtube.com/watch?v=", "");

    // var urlImage = "https://img.youtube.com/vi/" + idVideo + "/hqdefault.jpg";
    // var img = new Image();
    // img.src = urlImage;
    // img.alt = "Imagem em miniatura do vídeo";

    // boxVideo.append(img);
    boxVideo.on("click", function () {

        var iframe = document.createElement("iframe");

        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "");
        iframe.setAttribute("src", "https://www.youtube.com/embed/" + idVideo + "?rel=0&showinfo=0&autoplay=1");
        $(this).html('');
        $(this).append(iframe);
    });
}

(function () {
    'use strict';

    $(document).ready(function () {
        
        var url = window.location.protocol + "//" + window.location.host;

        var viewport = {
            w: $(window).width(),
            h: $(window).height()
        };

        $(window).resize(function () {
            viewport = {
                w: $(window).width(),
                h: $(window).height()
            };
        });

        var isMobile = function () {
            return (viewport.w < 768);
        };

        var isTablet = function () {
            return (viewport.w >= 768 && viewport.w <= 1024);
        };

        var isDesktop = function () {
            return (viewport.w > 1024);
        };

        var ie = msieVersion() ? msieVersion() : "";
        if (ie) {
            $("html").addClass("ie" + ie);
        }

        $("a[href='#']").click(function (e) {
            e.preventDefault();
        });

        $("[data-bg]").each(function () {
            $(this).css("background-image", "url('" + $(this).data('bg') + "')");
            $(this).css("background-repeat", "no-repeat");
        });

        $('.ico-menu').on('click', function(){
            
            $(this).hide();
            $('.close-menu').fadeIn();
            $('.header_menu').fadeIn();
                
            $('.close-menu').on('click', function(){
                $('.ico-menu').fadeIn();
                $(this).hide();
                $('.header_menu').fadeOut();
            });

            $('.header_menu a').on('click', function(){
                $('.ico-menu').fadeIn();
                $('.close-menu').hide();
                $('.header_menu').fadeOut();
            });
        });

        $('.player .icon-play').on('click',function(){
            $('.player_content').hide();
            $('.player_box').fadeIn();
            var mediaElement = document.getElementsByTagName("audio")[0];
            mediaElement.play();
        })

        //MASCARAS
        $("input[data-mask='telefone']").focusout(function () {
            var phone, element;
            element = $(this);
            element.unmask();
            phone = element.val().replace(/\D/g, '');
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        }).trigger('focusout');

        $('.carousel_eventos').slick({
            slidesToShow: 3,
            vertical: true,
            infinite: false,
            prevArrow: $('.evento-prev'),
            nextArrow: $('.evento-next')
        });

        $('.slider-musicas').slick({
            infinite: false,
            slidesToShow: 2,
            prevArrow: $('.musicas-prev'),
            nextArrow: $('.musicas-next')
        });

        $('.siganos-galeria').slick({
            infinite: false,
            slidesToShow: 5,
            prevArrow: $('.siganos-prev'),
            nextArrow: $('.siganos-next'),
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
        
        ancora();
        youtube();

    });
    
})();

$(function () { 
    $(window).scroll(function () {
        var adupla  = $('#aDupla').offset().top;
        var siganos = $('#sigaNos').offset().top;
        var footer  = $('#contato').offset().top;

        if($(this).scrollTop() >= siganos && $(this).scrollTop() <= adupla ){
            $('.logo-vermelha').hide();
            $('.logo-black').show();
            $('.header_menu-buttom').addClass('ativo');
        }else if($(this).scrollTop() >= footer){
            $('.logo-vermelha').hide();
            $('.logo-black').show();
            $('.header_menu-buttom').addClass('ativo');
        }else{
            $('.logo-vermelha').show();
            $('.logo-black').hide();
            $('.header_menu-buttom').removeClass('ativo');
        }
    })
});
