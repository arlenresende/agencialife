<?php require 'header.php'; ?>
<?php

// Get ID Deputados
if ( !file_exists("json-files/deputados-em-exercicio.json") ){
    $json = file_get_contents('http://dadosabertos.almg.gov.br/ws/deputados/em_exercicio?formato=json');
    $json = json_decode($json, true);

    if( isset($json) ){
        $fp = fopen('json-files/deputados-em-exercicio.json', 'w');
        fwrite($fp, json_encode($json));
        fclose($fp);
    }else {
        echo "<h2> Falha na requisição </h2>";
    }
    
}

$idDeputados = file_get_contents('json-files/deputados-em-exercicio.json');
$idDeputados = json_decode($idDeputados, true);

if (isset($idDeputados)) {
    $deputados = array();
    
    $names = array();

    foreach ($idDeputados['list'] as $i => $idDeputado) {

        $names[$idDeputado['id']] = "";
        $names[$idDeputado['id']] = $idDeputado['nome'];

        for ($meses = 1; $meses <= 10; $meses++) {
            // Get Verbas Indenizatorias 
            if ( !file_exists('json-files/deputados-verbas-'.$idDeputado['id'].'-'.$meses.'.json') ) :

                $json_verbas = file_get_contents('http://dadosabertos.almg.gov.br/ws/prestacao_contas/verbas_indenizatorias/deputados/' . $idDeputado['id'] . '/2019/' . $meses . '?formato=json');
                $json_verbas = json_decode($json_verbas, true);

                if( isset($json_verbas) ){
                    $fp = fopen('json-files/deputados-verbas-'.$idDeputado['id'].'-'.$meses.'.json', 'w');
                    fwrite($fp, json_encode($json_verbas));
                    fclose($fp);
                }else {
                    echo "<h2> Falha na requisição </h2>";
                }
            endif;

            $reembolsos = file_get_contents('json-files/deputados-verbas-'.$idDeputado['id'].'-'.$meses.'.json');
            $reembolsos = json_decode($reembolsos, true);
            
            $new_arr = [];
            if(is_array($reembolsos)){
                foreach ($reembolsos['list'] as $reembolso => $v) {
                    $new_arr[$v['idDeputado']][] = $v;
                    $deputados[$meses][$v['idDeputado']] = 0;
            }
            }   

            foreach ($new_arr as $key => $val) {
                foreach ($val as $i => $infoDeputado) {
                    $deputados[$meses][$key] += $infoDeputado['valor'];
                }
                arsort($deputados[$meses]);
            }
        }
        
    }
}
?>

<main>
    <div class="pg-home">
        <section id="home" class="">
            <?php
            for ($val = 1; $val <= 10; $val++) { ?>
                <div class="grid-33">
                    <section class="module">

                        <h1 class="module__heading module__heading--a">
                            <?php
                                switch ($val) {
                                    case 1:
                                        echo "Janeiro";
                                        break;
                                    case 2:
                                        echo "Fevereiro";
                                        break;
                                    case 3:
                                        echo "Março";
                                        break;
                                    case 4:
                                        echo "Abril";
                                        break;
                                    case 5:
                                        echo "Maio";
                                        break;
                                    case 6:
                                        echo "Junho";
                                        break;
                                    case 7:
                                        echo "Julho";
                                        break;
                                    case 8:
                                        echo "Agosto";
                                        break;
                                    case 9:
                                        echo "Setembro";
                                        break;
                                    case 10:
                                        echo "outubro";
                                        break;
                                }
                                ?>
                        </h1>

                        <ol class="custom-bullet custom-bullet--a">
                            <?php 
                                $cont = 0;
                                foreach ( $deputados[$val] as $i => $deputado) {  ?>   
                                    <?php if( $cont < 5 ) : ?>
                                    <?php echo "<li> Nome: " . $names[$i] . ',  Valor : R$' . number_format($deputado, 2, ',', '.') .  "</li>"; ?>
                                    <?php endif; $cont++;
                                }  ?> 
                        </ol>
                    </section>
                </div>
            <?php  } ?>
        </section>
    </div>
</main>
<?php require 'footer.php'; ?>