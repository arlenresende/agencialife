# Nome do produto
>Teste Técnico

[![NPM Version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url]

Mostrar os top 5 deputados que mais pediram reembolso de verbas indenizatórias por mês,
considerando somente o ano de 2019.

```sh
Arquivo index.php
```

Mostrar o ranking das redes sociais mais utilizadas dentre os deputados, ordenado de
forma decrescente.

```sh
Arquivo social.php
```

![](../header.png)


## Tecnologias


O projeto é baseado principalmente nas seguintes tecnologias:

PHP - Backend

Json

NPM - Dependencies manager

SASS - CSS preprocessor

Babel - ES6 JS compiler

Gulp - Task runner


## Compile

Como instalar o GULP


OS X ,Linux & Windows:

```sh
npm install gulp
```


gulp - Compiles all SCSS and JS files at once 

_Para mais exemplos, consulte a documentacao em (<https://gulpjs.com/>)


## Configuração para Desenvolvimento


Para poder usar ou modificar este projeto, você precisa:
Em um terminal, acesse o diretório do arquivo e execute o npm install para descarregar todas as dependências do projeto.
Com tudo instalado agora, você pode ir para o ramo desejado e fazer o que quiser dentro do projeto :)

Clone o repositório em sua máquina.

Altere o arquivo gulpfile.js em 

```sh
  proxy: "http://agencialife.local/"
```

para a url criada na sua configuração.

```sh
npm Install
```

## Histórico de lançamentos


    * Trabalho em andamento

## Meta


## Contributing

1. Faça o _fork_ do projeto (<https://github.com/yourname/yourproject/fork>)
2. Crie uma _branch_ para sua modificação (`git checkout -b feature/fooBar`)
3. Faça o _commit_ (`git commit -am 'Add some fooBar'`)
4. _Push_ (`git push origin feature/fooBar`)
5. Crie um novo _Pull Request_

[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/seunome/seuprojeto/wiki
