<?php require 'header.php'; ?>
<?php

// Get ID Deputados

?>
<main>
    <div class="pg-home">
    <section id="home" class="">
        <div class="grid-100">
            <div class="grid-33">
                <section class="module">

                    <h1 class="module__heading module__heading--b">Midias Sociais</h1>

                    <ol class="custom-bullet custom-bullet--b">
                        <?php

                        $getMidias = file_get_contents('json-files/midias-sociais.json');
                        $jsonMidas = json_decode($getMidias, true);

                        $listDepts = $jsonMidas['list'];
                        $midiasSocias = array();
                        $cont = 0;
                     


                        foreach ($listDepts as $val) {
                            $midias = array();

                            foreach ($val['redesSociais'] as $k => $rs) {
                                $midias[$k]   = $rs['redeSocial']['nome'];
                                $midiasSocias[$cont] = $rs['redeSocial']['nome'];
                                $cont++;
                            }
                        }
                        
                        $foo = array_count_values($midiasSocias);
                        asort($midiasSocias);
                        foreach ($foo as $key => $valor) {
                            echo "<li> Midia: " . $key . " - Utilizam: " . $valor . "<br> <hr> </li>";
                        }
                        ?>
                    </ol>

                </section>
            </div>
        </div>

    </section>
    </div>


</main>
<?php require 'footer.php'; ?>