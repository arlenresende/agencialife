﻿(function ($) {
    var oAddClass = $.fn.addClass;
    $.fn.addClass = function () {
        for (var i in arguments) {
            var arg = arguments[i];
            if ( !! (arg && arg.constructor && arg.call && arg.apply)) {
                setTimeout(arg.bind(this));
                delete arguments[i];
            }
        }
        return oAddClass.apply(this, arguments);
    };

})(jQuery);

function msieVersion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0) /* If Internet Explorer, return version number */
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    else /* If another browser, return 0 */
        return 0;
}
function ie() {
    var ua = window.navigator.userAgent,
      msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }
    return false;
}

function ancora(){
    var off;
    var $doc = $('html, body');
    $('.ancora').click(function(e) {
        off = $(this).data('ancora') ? $(this).data('ancora') : 0;

        $doc.animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top - off
        }, 800);
        return false;
    });
}

function youtube(){
        
    var boxVideo = $(".youtube_video");
    var idVideo = boxVideo.data('video').replace("https://www.youtube.com/watch?v=", "");

    // var urlImage = "https://img.youtube.com/vi/" + idVideo + "/hqdefault.jpg";
    // var img = new Image();
    // img.src = urlImage;
    // img.alt = "Imagem em miniatura do vídeo";

    // boxVideo.append(img);
    boxVideo.on("click", function () {

        var iframe = document.createElement("iframe");

        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "");
        iframe.setAttribute("src", "https://www.youtube.com/embed/" + idVideo + "?rel=0&showinfo=0&autoplay=1");
        $(this).html('');
        $(this).append(iframe);
    });
}
